<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\WarehouseTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenerateCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $number_of_transaction_in_same_date=0;
        $transactions=WarehouseTransaction::all();
        $transaction=new WarehouseTransaction;
        $transaction->transaction_type_name=$request->transaction_type_name;
        $transaction->created_date=$request->created_date;
        foreach(range(1,count($transactions)) as $i){
         $transaction=$transaction[$i];
         if($request->created_date===$transaction->created_date){
            $number_of_transaction_in_same_date=$number_of_transaction_in_same_date +1;
         }
        }

        $transaction->number_of_transaction_in_same_date=$number_of_transaction_in_same_date;
        $transaction->save();
    }

    /**
     * Display the specified resource.
     */
    public function generateCode(string $id)
    {
        $item=Item::find($id);
        $first_char=substr($item->name,1);
        $second_char=substr($item->category,1);
        $third_char=substr($item->category,-1);
        $fourth_char=substr($item->commercial_name,1);
        $str_length=strlen($item->commercial_name);
        $zero_or_number=0;
        if($str_length>9){
        $three_digits=0 . $str_length;
        }
        else{
            $three_digits=0.0.$str_length;
        }
        $item->code=$first_char.$second_char.$third_char.$fourth_char.$three_digits;
    }

    /**
     * Update the specified resource in storage.
     */
    public function reverseCode(Request $request)
    {
        $item=DB::table('items')->where('code',$request->code)->select('name','commercial_name','item_category')->all();

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
