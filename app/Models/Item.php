<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable=['name','commercial_name',"code","category_id"];
    protected $hidden=['created_at','updated_at',];

    public function category(){
        return $this ->belongsTo('App\Models\Category','category','name');
    }

}
